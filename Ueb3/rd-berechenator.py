import random as r
from scipy.special import comb
import numpy as np
import matplotlib.pyplot as plt
import math

r.seed(0)

def distortion(M, p = .8):
    out = 0
    for i in range(M):
        out += comb(M,i) * p**(M-i) * (1-p)**(i) * (M - np.abs(M-i))
    return out

def rate(M, entropy = 1):
    return entropy/M

def chunks(l, n):
    """Yield successive n-sized chunks from l. Quelle: Stackoverflow Ned Batchelder"""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def generateRandomSeqence(p0 = 0.8, length = 10):
    list = []
    for _ in range(length):
        list.append(0 if r.uniform(0,1) <= p0 else 1)
    return list

def error(block1, block2):
    sum = 0
    for i in range(len(block1)):
        sum += block1[i] ^ block2[i]
    return sum

class berechenator:
    def __init__(self,M = 1):
        self.M = M

    def encode(self, code):
        outlist = []
        for chunk in chunks(code, self.M):
            outlist.append(0 if chunk.count(0) >= self.M / 2 else 1)
        return outlist

    def decode(self, code):
        outlist = []
        for bit in code:
            outlist.extend([bit] * self.M)
        return outlist


def entropy(p0 = 0.8):
    return (-p0*math.log(p0,2)-(1-p0)*math.log((1-p0),2))

def binaryRD(D, p0 = 0.8):
    if(D > 0 and D < min(p0, 1-p0)):
        return entropy(p0) - entropy(D)
    return 0





def main():
    m = [2**i for i in range(5)]
    x = [distortion(x) for x in m]
    y = [rate(y) for y in m]
    y2= [rate(y, entropy(.8)) for y in m]
    #print(x)
    #print(y)
    #print(y2)

    plt.subplot(211)
    plt.xlabel("Distortion")
    plt.ylabel("Rate")
    plt.plot(x,y, label="R(D) of 2 a)")
    plt.plot(x,y2,label="R(D) of 2 b)")
    plt.plot(np.arange(0,.2,.01), [binaryRD(x) for x in np.arange(0,.2,.01)], label="R(D) binary addition")
    plt.legend()

    x2 = []
    iterations = 1000
    for i in m:
        b = berechenator(i)
        err = []
        for j in range(iterations):
            code = generateRandomSeqence(.8,i)
            out = b.decode(b.encode(code))
            err.append(error(code, out))
            #print("Err of m:" + str(i) + " j: " + str(j) + " = " + str(err))
        x2.append(np.mean(err))
    print("X2:" +str(x2))
    plt.subplot(212)
    plt.xlabel("Distortion")
    plt.ylabel("Rate")
    plt.plot(x2, y, label="Empirical mean R(D) over "+str(iterations)+" of 2 c)")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
