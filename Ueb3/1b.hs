module Main where

-- Requires library 'boxes'.
-- Install with: `cabal update; cabal install boxes`
import Text.PrettyPrint.Boxes

import Numeric

pyx :: [[Double]]
pyx = [ [ 1
        , 1
        , 0.2
        , 0
        , 0
        , 0
        ]
      , [ 0
        , 0
        , 0.8
        , 0
        , 0
        , 0
        ]
      , [ 0
        , 0
        , 0
        , 1
        , 0.4
        , 0
        ]
      , [ 0
        , 0
        , 0
        , 0
        , 0.4
        , 1
        ]
      ]

pxy :: [[Double]]
pxy = [ [ 5/18
        , 0
        , 0
        , 0
        ]
      , [ 5/9
        , 0
        , 0
        , 0
        ]
      , [ 1/6
        , 1
        , 0
        , 0
        ]
      , [ 0
        , 0
        , 5/6
        , 0
        ]
      , [ 0
        , 0
        , 1/6
        , 1/4
        ]
      , [ 0
        , 0
        , 0
        , 5/8
        ]
      ]


py :: [Double]
py = [ 0.3
     , 0.2
     , 0.3
     , 0.2
     ]

px :: [Double]
px = [ 1/12
     , 1/6
     , 3/12
     , 1/4
     , 1/8
     , 1/8
     ]

hXy' :: [[Double]] -> Int -> Double
hXy' pxy_ y = negate . sum . filter (not . isNaN) . map (sumBody y) $ [0..(length pxy_ - 1)]
    where sumBody :: Int -> Int -> Double
          sumBody y_ x = (pxy_ !! x !! y_) * log2 (pxy_ !! x !! y_)

hXy :: Int -> Double
hXy = hXy' pxy

hYx :: Int -> Double
hYx = hXy' pyx

hXY' :: [Double] -> (Int -> Double) -> Double
hXY' py_ hXy_ = sum . map sumBody $ [0..3]
    where sumBody :: Int -> Double
          sumBody y = py_ !! y * hXy_ y

hXY :: Double
hXY = hXY' py hXy

hYX :: Double
hYX = hXY' px hYx

hXyTable :: Box
hXyTable = vcat top $ header : (row <$> [1..4]) ++ [emptyBox 3 0, footer]
    where header :: Box
          header = text "y" <> emptyBox 0 5 <> text "H(X|y)" //
                   text "------------"
          
          row :: Int -> Box
          row y = int y <> text "  -  " <> double (hXy (y-1))

          footer :: Box
          footer = text "H(X|Y) =" <+> double hXY

hYxTable :: Box
hYxTable = vcat top $ header : (row <$> [0..5]) ++ [emptyBox 1 0, footer]
    where header :: Box
          header = text "x" <> emptyBox 0 5 <> text "H(Y|x)" //
                   text "------------"

          row :: Int -> Box
          row x = int x <> text "  -  " <> double (hYx x)

          footer :: Box
          footer = text "H(Y|X) =" <+> double hYX

output :: Box
output = hXyTable <> emptyBox 0 5 <> hYxTable

main :: IO ()
main = putStr $ render output

double :: Double -> Box
double = text . ($ "") . showFFloat (Just 2)

int :: Int -> Box
int = text . show

log2 :: Floating a => a -> a
log2 = logBase 2
