import string
import queue
import math
from collections import Counter


class HuffNode(object):
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.frequency = left.frequency + right.frequency

    def children(self):
        return((self.left, self.right))

    def isLeaf(self):
        return False

    def getPriority(self, maxFreq):
        return self.frequency

    def __lt__(self, x):
        if x.isLeaf():
            return x < self
        else:
            return self.frequency < x.frequency

    def toCodeList(self, prefix=""):
        codelist = []
        l = self.left.toCodeList(prefix + "0")
        r = self.right.toCodeList(prefix + "1")
        codelist.extend(l)
        codelist.extend(r)
        return codelist

    def toCodeFrequencyList(self, prefix=""):
        codelist = []
        l = self.left.toCodeFrequencyList(prefix + "0")
        r = self.right.toCodeFrequencyList(prefix + "1")
        codelist.extend(l)
        codelist.extend(r)
        return codelist


class HuffLeaf(HuffNode):
    def __init__(self, frequency, char):
        self.char = char
        self.frequency = frequency

    def isLeaf(self):
        return True

    def __lt__(self, x):
        if x.isLeaf():
            return self.frequency < x.frequency or (self.frequency == x.frequency and self.char < x.char)
        else:
            return self.frequency < x.frequency

    def toCodeList(self, prefix=""):
        return [(prefix, self.char)]

    def toCodeFrequencyList(self, prefix=""):
        return [(prefix, self.frequency)]


class HuffTree(object):
    def __init__(self, frequencies):
        self.fr = frequencies
        self.maxFreq = max([freq for (_, freq) in frequencies])
        self.root = self.__generateTree()

    def __generateTree(self):
        leaves = [HuffLeaf(freq, char) for (char, freq) in self.fr]

        pq = queue.PriorityQueue()
        for leaf in leaves:
            pq.put((leaf.getPriority(self.maxFreq), leaf))

        while pq.qsize() > 1:
            (_, l) = pq.get()
            (_, r) = pq.get()
            node = HuffNode(l, r)
            pq.put((node.getPriority(self.maxFreq), node))
        return pq.get()[1]

    def toCodeList(self):
        return self.root.toCodeList()

    def toCodeFrequencyList(self):
        return self.root.toCodeFrequencyList()


class Huffmensch2(object):
    def __init__(self, symbols, code=None, chunksize=1):
        self.code = code
        self.symbols = symbols
        self.chunksize = chunksize
        if self.code is None:
            self.code = self.generateCode(symbols)

    def encode(self, text):
        cryptotext = "".join([self.findInTupellistSecond(s, self.code) for s in chunks(self.chunksize, text)])
        return cryptotext

    def decode(self, chiffre):
        if self.code is None:
            print("Tried to decode without generated Code!")
            return
        text = chiffre
        plain = ""
        while len(text) > 1:
            found = False
            for (codeword, plainword) in self.code:
                if text.startswith(codeword):
                    plain = plain + plainword
                    text = text[len(codeword):]
                    found = True
                    break
            if not found:
                print("Unknown codeword. Rest text: %s" % text)
                break
        return plain

    def findInTupellistFirst(self, elem, tupellist):
        for (f, s) in tupellist:
            if elem == f:
                return s
        return None

    def findInTupellistSecond(self, elem, tupellist):
        for (f, s) in tupellist:
            if elem == s:
                return f
        return None

    def generateCode(self, text):
        frequency = sorted(Counter(chunks(self.chunksize, text)).items())
        self.tree = HuffTree(frequency)
        return self.tree.toCodeList()

    def generateCodeToProbabilityList(self):
        cf = self.tree.toCodeFrequencyList()
        s = 0
        for (c,f) in cf:
            s += f 
        return [(p, f/s) for (p, f) in cf]



def chunks(n, s):
    return [s[i:i+n] for i in range(0, len(s), n)]

def meanWordlen(probList):
    l = 0
    for (code, probab) in probList:
        l += len(code) * probab #this utilizes the fact that the sum of all probabilities is 1.
    return l



def main(chunksize=1):
    text_file = open("rfc791.txt", "r")
    text = text_file.read()
    h = Huffmensch2(text, chunksize=chunksize)
    code = h.encode(text)
    #print("Encoded: %s" % code)
    codelen = len(code)/8.
    textlen = len(text)
    print("Size of code: " + str(codelen) + " Byte")
    print("Size of text: " +  str(textlen) + " Byte")
    print("Improvement: " + str(textlen / codelen * 100) + "%")

    numberOfSymbols = len(h.code)
    entscheidungsGehalt = math.log(len(h.code),2)
    probList = h.generateCodeToProbabilityList()
    entropy = - sum([p * math.log(p, 2) for (_, p) in probList])
    meanWordl = meanWordlen(probList)

    print("Entscheidungsgehalt: " + str(entscheidungsGehalt))
    print("Mean wordlength: " + str(meanWordl))
    print("Entropy: " + str(entropy))
    print("Redundancy: " + str(meanWordl - entropy))
    #decoded = h.decode(code)
    #print("Decoded: %s" % decoded)


if __name__ == "__main__":
    print("Chunksize 1:")
    main(1)
    print("Chunksize 2:")
    main(2)
