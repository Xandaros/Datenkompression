#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import numpy as np
import scipy.misc
import matplotlib.pyplot as plt


class TransEnc(object):

    def __init__(self, data):
        self.data = data

    def subtract_mean(self):
        self.mean = np.matrix(self.data).mean(1)
        self.pdata = np.matrix(self.data - self.mean)

    def add_mean(self):
        self.fdata = np.matrix(self.untransformed + self.mean)

    def cov(self):
        # data = np.matrix(self.pdata)
        # tdata = data.getT()
        # self.cov = 1/len(data[0]) * data * tdata
        self.cov = self.pdata * self.pdata.transpose() / (self.pdata.shape[1] - 1)

    def eig(self):
        self.eigenvalues, self.V = np.linalg.eig(self.cov)

    def sort_vectors(self):
        v = self.V.getT()
        self.sorted = list(zip(self.eigenvalues, np.array(v)))
        self.sorted.sort(key=lambda xs: xs[0], reverse=True)
        self.sorted = np.matrix([x[1] for x in self.sorted])
        self.sorted = self.sorted.getT()

    def cut_to_dimensions(self, dimensions):
        self.sorted = self.sorted[:, range(dimensions)]

    def encode(self, data, indicesBasis=0):
        if indicesBasis > 0:
            self.cut_to_dimensions(indicesBasis)
        return self.sorted.getT() * np.matrix(data).getT()

    def decode(self, data, indicesBasis=0):
        if indicesBasis > 0:
            self.cut_to_dimensions(indicesBasis)
        return self.sorted * np.matrix(data)


def getBasis(paths):
    def loadImage(path):
        image2d = scipy.misc.imread(path, True)
        return np.array(np.reshape(image2d, (1, -1))[0])
    images = [loadImage(path) for path in paths]
    pca = TransEnc(images)
    pca.subtract_mean()
    pca.cov()
    pca.eig()
    pca.sort_vectors()
    return pca.sorted


def encode(data, indicesBasis):
    return data.getT() * indicesBasis.getT()


def get_mse(paths, dimensions=0):
    def loadImage(path):
        image2d = scipy.misc.imread(path, True)
        return np.array(np.reshape(image2d, (1, -1))[0])
    images = [loadImage(path) for path in paths]
    pca = TransEnc(images)
    pca.subtract_mean()
    pca.cov()
    pca.eig()
    pca.sort_vectors()
    if dimensions > 0:
        pca.cut_to_dimensions(dimensions)
    pca.transform()
    pca.transform_inv()
    pca.add_mean()
    mse = np.square(pca.fdata - pca.data).mean(axis=None)
    return mse


def plot_mse(paths):
    mses = [get_mse(paths, i+1) for i in range(len(paths))]
    plt.plot(mses, 'b')
    plt.show()


def generate_new_image(paths, dimensions=0):
    def loadImage(path):
        image2d = scipy.misc.imread(path, True)
        return np.array(np.reshape(image2d, (1, -1))[0])
    images = [loadImage(path) for path in paths]
    pca = TransEnc(images)
    pca.subtract_mean()
    pca.cov()
    pca.eig()
    pca.sort_vectors()
    if dimensions > 0:
        pca.cut_to_dimensions(dimensions)
    pca.transform()
    pca.transform_inv()
    pca.add_mean()
    return np.array(pca.fdata[0])


def generate_new_images(paths):
    images = [generate_new_image(paths, dim+1) for dim in range(36)]
    i = 1
    for image in images:
        restoredImage = np.reshape(image, (64, 64))
        scipy.misc.imsave("output/" + str(i) + ".pgm", restoredImage)
        i += 1


if __name__ == "__main__":
    # cols: sample
    # rows: variable
    # data = np.array([ #[1.1, 1.0, 1.1, 1.0, 1.1, 1.2, 1.11, .9, 1.01, 1.1],
    #     [2.4, 0.7, 2.9, 2.2, 3.0, 2.7, 1.6, 1.1, 1.6, 0.9],
    #     [2.5, 0.5, 2.2, 1.9, 3.1, 2.3, 2.0, 1.0, 1.5, 1.1]
    #                   ])
    # pca = myPCA(data)
    # pca.subtract_mean()
    # #print pca.pdata
    # pca.cov()
    # #print pca.cov
    # pca.eig()
    # pca.sort_vectors()
    # pca.transform()
    # pca.transform_inv()
    # print(pca.ev)
    # print(pca.pc)
    files = []
    for (path, _, filenames) in os.walk("dataset"):
        files.extend([path + "/" + filename for filename in filenames])
        break
    plot_mse(files)
    generate_new_images(files)
    # basis = getBasis(files)
    # print(basis)
