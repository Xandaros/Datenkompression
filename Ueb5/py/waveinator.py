#!/usr/bin/env python3 -W ignore::DeprecationWarning
# -*- coding: utf-8 -*-
import numpy as np
import math
import matplotlib.pyplot as plt
import array
#import wave
import soundfile as sf
import coder
import quantizer
import struct
import collections

"""ASSUMING 24 Bit WAVE"""
"""DID YOU JUST ASSUME MY WAVEFORM?"""

class waveynator():
    def __init__(self, buckets = (2**2)+1, maxPeak=1.):
        val = buckets/((2**3))
        if(val > 2):
            val = 2
        print("Using " + str(val*100) + " Percent of maximum peak " + str(maxPeak) + " at " + str(buckets) + " quantizer levels.")
        self.min_val = -val*maxPeak
        self.max_val =  val*maxPeak
        self.mu      =  500
        self.buckets= buckets
        if(buckets < 2**2):
            self.encoderw= [0.5, 0.5]
        elif(buckets < 2**4):
            self.encoderw= [0.25, 0.25, 0.5]
        else:
            self.encoderw= [1.]
        print("Coder is using Weight: " + str(self.encoderw))

        self.coder = coder.DiffEnc(quantizer.Compander(self.mu, self.min_val, self.max_val, self.buckets), self.encoderw)

    def encode(self, samples):
        return self.coder.encode(samples)

    def decode(self, code):
        return self.coder.decode(code)

    """Fist weight is the nearest"""
    def decodeLowpass(self, code, weights=[0.4, 0.6]):
        wave = self.decode(code)
        buf = [wave[0]] * len(weights)
        out = []
        for i in range(len(wave)):
            val = []
            #print("buf: " + str(buf) + " ,i: " + str(i))
            for j in range(len(weights)):
                val.append(buf[j] * weights[j])
            #print("val: " + str(val))
            out.append(sum(val))
            #print("buf[:-1]: " + str(buf[:-1]), "wave[i]: " + str(wave[i]))
            buf.pop()
            buf.insert(0, wave[i])

        return out

    def calcEntropy(self, code):
        dings = collections.Counter(code)
        vorkommen = [dings[x] / len(code) for x in range(self.buckets)]
        entropy = sum([-v * math.log(v,2) if v > 0 else 0 for v in vorkommen])
        return entropy 

def loadWave(path):
    (out, info) = sf.read(path)
    if type(out[0]) == np.ndarray:
        ret = list(map(np.array, zip(*out)))
        return (ret, info)
    else:
        return ([out], info)

def saveWave(path, samples, samplerate):
    if(len(samples) > 1):
        out = list(zip(samples[0], samples[1]))
    else:
        out = samples[0]
    sf.write(path, out, samplerate, subtype='PCM_24')

def overlayRandomWaves(count):
    (samples,info) = generateRandomWave()
    for _ in range(count):
        (s, _) = generateRandomWave()
        samples += s
    samples = [i / count for i in samples]
    return (np.array(samples), info)

def generateRandomWave():
    amplitude = 2**8
    length = 500
    samplerate = 44100
    frequency =np.random.random_sample()*100*np.pi
    #return (generateWave(amplitude, length, frequency), (1, samplewidth, samplerate, length, 'NONE', 'not compressed'))
    return (generateWave(amplitude, length, frequency), samplerate)

def generateWave(amplitude, length, frequency):
    t = np.linspace(0, frequency, length)
    return amplitude * np.sin(t)

def testWave():
    inp = "audio/audio1.wav"
    oup = "audio/1.out.wav"
    (samples, info) = loadWave(inp)
    saveWave(oup, samples, info)
    
def main(inputpath="audio/audio1.wav", quantizerLevels=9, showLog=False):
    print("Loading wave from "+inputpath)
    outputpath = inputpath.replace(".wav", "") + "_%0.1fbit_coded.wav" % np.log2(quantizerLevels)
    (samples, info) = loadWave(inp)
    print("Loaded wave, no. channels: "+str(len(samples))+", samplerate: " + str(info) + ", length: " + str(len(samples[0]) / info) + " sec.")
    #(samples, info) = overlayRandomWaves(5)
    print("Finding maximum Peak in all channels.")
    maxPeak = 0
    for c in samples:
        for s in c:
            if abs(s) > maxPeak:
                maxPeak = abs(s)
    print("Initing Quantizer and Coder.")
    w = waveynator(quantizerLevels, maxPeak)
    print("Encoding Wave with " + str(np.log2(quantizerLevels)) + " Bit/sample")
    codes = [w.encode(channel) for channel in samples]
    print("Calculating entropy per symbol")
    entropy = w.calcEntropy(codes[0])
    print(str(math.ceil(entropy)) + " bit ("+str(entropy)+")")
    print("Decoding Wave.")
    #[0.02, 0.05, 0.08, 0.15, 0.3, 0.4]
    #[0.16, 0.16, 0.16, 0.16, 0.18, 0.18]
    weights = [.3,.3, .4]
    if(quantizerLevels >= 2**9):
        weights = [.5,.5]
    print("Using low-pass weights of: " + str(weights))
    output = [w.decodeLowpass(code, weights) for code in codes]
    #output = w.decode(code)
    mse = sum((samples[0] - output[0])**2)/float(len(samples))
    print("Calculated mean squared error: " + str(mse))
    if (showLog):
        inter = [w.coder.q.partition[int(i)] / ((w.coder.q.codebook[-1] - w.coder.q.codebook[0])/2.) + w.coder.q.codebook[0] for i in codes[0][len(w.encoderw):]]
        plt.plot(samples[0], label="original", alpha=.4)
        plt.plot(output[0], label="compressed", alpha=.6)
        plt.plot(samples[0] - output[0], label="Error", alpha=0.2)
        plt.plot(inter, label="intern", alpha=.3)
        plt.legend()
        plt.tight_layout()
        plt.show()

    print("Save wave at " + outputpath)
    saveWave(outputpath, output, info)
    return (np.log2(quantizerLevels), mse, math.ceil(entropy), entropy)
    

if __name__ == "__main__":
    import sys
    if(len(sys.argv) >= 2):
        inp = sys.argv[1]
    else:
        inp = "audio/audio1.wav"
        
    rd = []    
    if(len(sys.argv) >= 3):
        log = len(sys.argv) == 4
        main(inp, int(sys.argv[2]), log)
        sys.exit()
        
    for i in range(2, 6):
        print("Round " + str(i-1) + ":\n")
        rd.append(main(inp, i))
        
    for i in range(3,8):
        print("Extra round " + str(2**(i+1)) + ":\n")
        rd.append(main(inp, 2**(i+1)))
    
    plt.title(inp)
    plt.xlabel("Rate in bit/sample")
    plt.ylabel("Distortion as mean squared error")
    rate = [x[0] for x in rd]
    distortion = [x[1] for x in rd]
    trueentropy= [x[2] for x in rd]
    fakeentropy= [x[3] for x in rd]
    plt.plot(rate, distortion, label="Distortion")
    plt.plot(rate, trueentropy, label="True entropy per Symbol")
    plt.plot(rate, fakeentropy, label="Theoretical entropy per Symbol")
    plt.legend()
    #plt.subplots(0).set_xlim(xmin=1)
    #plt.subplots(0).set_ylim(ymin=0)
    plt.show()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
