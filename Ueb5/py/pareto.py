import math

import numpy as np
from matplotlib import cm
import matplotlib.pyplot as pl
import mpl_toolkits.mplot3d


def u(x1, x2, k1, c, k2, a):
    if x1 + c == 0:
        return -math.inf
    return k1*math.log(x1 + c) + k2*x2**a


def uA(x1, x2):
    return u(x1, x2, 1, 20, 0.5, 1.01)


def uB(x1, x2):
    return u(x1, x2, 1, 0, 0.5, 1.01)


def uA4(x1a, x2a, x1b, x2b):
    return uA(x1a, x2a)


def uB4(x1a, x2a, x1b, x2b):
    return uB(x1b, x2b)


def uO(x1a, x2a, x1b, x2b):
    return uA(x1a, x2a) + uB(x1b, x2b)


def plot():
    G1 = 100
    G2 = 500
    x = list(range(1, G1))
    y = list(range(1, G2))
    X, Y = np.meshgrid(x, y)
    z1 = np.array([uA(x1, x2) for x1, x2 in zip(np.ravel(X), np.ravel(Y))])
    Z1 = z1.reshape(X.shape)
    z2 = np.array([uB(G1 - x1, G2 - x2) for x1, x2 in zip(np.ravel(X), np.ravel(Y))])
    Z2 = z2.reshape(X.shape)
    z3 = np.array([uA(x1, x2) + uB(G1 - x1, G2 - x2) for x1, x2 in zip(np.ravel(X), np.ravel(Y))])
    Z3 = z3.reshape(X.shape)
    figure = pl.figure()
    axis = figure.add_subplot(131, projection='3d')
    axis.plot_surface(X, Y, Z1, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    axis.set_title("$u_A$")
    axis = figure.add_subplot(132, projection='3d')
    axis.plot_surface(X, Y, Z2, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    axis.set_title("$u_B$")
    axis = figure.add_subplot(133, projection='3d')
    axis.plot_surface(X, Y, Z3, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    axis.set_title("$u_A + u_B$")
    pl.show()


def paretoOptimum(func=uO):
    x1a = 0
    x2a = 0
    x1b = 100
    x2b = 500

    value = func(x1a, x2a, x1b, x2b)

    while True:
        testVal = func(x1a - 1, x2a, x1b + 1, x2b)
        if x1a > 0 and testVal > value:
            value = testVal
            x1a -= 1
            x1b += 1
            continue
        testVal = func(x1a + 1, x2a, x1b - 1, x2b)
        if x1b > 0 and testVal > value:
            value = testVal
            x1a += 1
            x1b -= 1
            continue
        testVal = func(x1a, x2a - 1, x1b, x2b + 1)
        if x2a > 0 and testVal > value:
            value = testVal
            x2a -= 1
            x2b += 1
            continue
        testVal = func(x1a, x2a + 1, x1b, x2b - 1)
        if x2b > 0 and testVal > value:
            value = testVal
            x2a += 1
            x2b -= 1
            continue
        break
    return x1a, x2a, x1b, x2b, value


if __name__ == '__main__':
    print("Pareto-Optimum for A: " + str(paretoOptimum(uA4)))
    print("Pareto-Optimum for B: " + str(paretoOptimum(uB4)))
    print("Pareto-Optimum for both: " + str(paretoOptimum(uO)))
    plot()
