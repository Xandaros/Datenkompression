import math
import random
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata

def drange(x, y, jump=1):
    while x < y:
        yield float(x)
        x += jump

random.seed(1)


class BinarySampler:
    def __init__(self, p_1):
        self.p_1 = p_1
        self.epsilon = .1

    def generate_samples(self, n, str_len):
        out = []
        for _ in range(n):
            strin = []
            for _ in range(str_len):
                char = 1 if (random.uniform(0, 1) < self.p_1) else 0
                strin.append(char)
            out.append(strin)
        return out

    def checkMic(self, n):
        flat = [item for sublist in n for item in sublist]
        ons = flat.count(1)
        su = len(flat)

        print("One Ratio: " + str(ons / su))
        print("Should: " + str(self.p_1))

    def getEntropyPerSymbol(self):
        return - (self.p_1*math.log(self.p_1, 2) + (1-self.p_1) * math.log(1 - self.p_1, 2))

    def getProbabilityOfSequence(self, sequence):
        p = 1
        for elem in sequence:
            p *= self.p_1 if elem == 1 else (1-self.p_1)
        return p

    def isInTypicalSet(self, sequence):
        p = self.getProbabilityOfSequence(sequence)
        entropy_l = 2**(-len(sequence)*(self.getEntropyPerSymbol()+self.epsilon))
        entropy_h = 2**(-len(sequence)*(self.getEntropyPerSymbol()-self.epsilon))
        #print("Lower bound: " + str(entropy_l))
        #print("Higher bound: " + str(entropy_h))
        # print("Probility of sequence: " + str(p))

        return entropy_l < p and p < entropy_h

    def getTypicalSet(self, samples):
        ts = []
        for sequence in samples:
            if(self.isInTypicalSet(sequence)):
                ts.append(sequence)
        return ts

    def normalizedKumulativeInformationdensity(self, samples):
        samples_no = len(samples)
        sequence_len = len(samples[0]) + 1
        histo = [0] * sequence_len

        for sequence in samples:
            histo[sequence.count(1)] += math.log(1 / self.getProbabilityOfSequence(sequence), 2)

        for i in range(sequence_len):
            histo[i] /= samples_no

        return histo

    def informationDensity(self, sequence):
        if self.getProbabilityOfSequence(sequence) == 0:
            return 0
        return -math.log(self.getProbabilityOfSequence(sequence), 2)


def relativeProbability(samples):
    samples_no = len(samples)
    sequence_len = len(samples[0]) + 1
    histo = [0 for _ in range(sequence_len)]

    for sequence in samples:
        histo[sequence.count(1)] += 1.

    for i in range(sequence_len):
        histo[i] /= samples_no

    return histo


if __name__ == "__main__":
    bs = BinarySampler(.3)
    samples = bs.generate_samples(100000, 10)
    # bs.checkMic(samples)
    histo = relativeProbability(samples)

    # print(samples[0])
    # print("Entropy per symbol: " + str(bs.getEntropyPerSymbol()))
    # print("P(sequence) = " + str(bs.getProbabilityOfSequence(samples[0])))
    # print(bs.isInTypicalSet(samples[0]))

    print(histo)
    print(len(histo))

    classes = []
    for i in range(0, len(samples[0])):
        x = [1] * i
        x.extend([0] * len(samples[0]))
        classes.append(x)

    idp = [bs.getProbabilityOfSequence(clss) for clss in classes]
    idi = [bs.informationDensity(clss) for clss in classes]
    print("IDS: " + str(idi))


    ts = bs.getTypicalSet(samples)
    tsr = relativeProbability(ts)
    tsi = bs.normalizedKumulativeInformationdensity(samples)

    f, axarr = plt.subplots(2, sharex=True)

    plt.xlabel("Number of ones")
    plt.grid(True)
    axarr[0].set_title('Relative Probability')
    axarr[1].set_title('Typical Set')
    #plt.ylabel("Occurences")

    x_axis = list(drange(-.5, len(histo)-.5))

    axarr[0].bar(x_axis, histo, label="Relative Probability")
    #axarr[1].bar(x_axis, tsr, label="Typical Set")
    #axarr[1].plot(tsi)
    #axarr[1].plot(tsh, color="red")

    #information Density
    axarr[1].plot(idp, color="red", label="Prob.")
    axarr[1].plot(idi, color="purple", label="Inf.d.")

    #bounds
    entropy_l = 2**(-len(samples[0])*(bs.getEntropyPerSymbol()+bs.epsilon))
    entropy_h = 2**(-len(samples[0])*(bs.getEntropyPerSymbol()-bs.epsilon))
    print("low: " + str(entropy_l))
    print("high: " + str(entropy_h))
    axarr[1].axhline(y=entropy_l, label="Lower Bound", color="blue")
    axarr[1].axhline(y=entropy_h, label="Higher Bound", color="orange")

    #middle
    middle = 2**(-len(samples[0])*bs.getEntropyPerSymbol())
    print("Middle = " + str(middle))
    axarr[1].axhline(y=middle,label="Middle",color="green")

    plt.xlim([-1, len(samples[0])])
    plt.ylim([0,entropy_h*2])
    plt.legend()
    plt.show()

    # axarr[0].histo([s.count(1) for s in ts], distibuted=True, width= 1)

    # plt.show()
