#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from quantizer import UniformQuantizer, Compander
import matplotlib.pyplot as pl
import math

class DifferentialEncoder(object):

    def __init__(self, quantizer):
        self.q = quantizer

    def encode(self, s):
        r = np.zeros(len(s))
        r_rec = np.zeros(len(s))

        for i in range(len(s)):
            if i == 0:
                r[0] = s[0]
                r_rec[0] = s[0]
            else:
                diff = s[i] - r_rec[i-1]
                diff_q = self.q.encode([diff])
                r[i] = diff_q[0]

                diff_dec = self.q.decode([r[i]])
                r_rec[i] = r_rec[i-1] + diff_dec[0]

        return r

    def decode(self, s):
        r = np.zeros(len(s))
        s_dec = self.q.decode(s[1:len(s)])
        r[0] = s[0]
        for i in range(1,len(s)):
            r[i] = r[i-1] + s_dec[i-1]
        return r
        
        
class DiffEnc(object):
    """Linear predictive differential Coder"""

    def __init__(self, quantizer, weights=[0,1]):
        self.q = quantizer
        self.w = weights
        
    def encode(self, s):
        r = np.zeros(len(s))
        r_rec = np.zeros(len(s))

        for i in range(len(s)):
            if i < len(self.w):
                r[i] = s[i]
                r_rec[i] = r[i]
            else:
                pred = 0
                for j in range(len(self.w)-1, -1, -1):
                    pred += self.w[j] * r_rec[i-1-j]
                    #print(str(pred) + " = " + str(self.w[j]) + " * " + str(r_rec[i-1-j]))
                    
                diff = s[i] - pred
                #print("r["+str(i) +"]: "+ str(s[i]) + " - " + str(pred) + " = " + str(diff))
                
                diff_q = self.q.encode([diff])
                r[i] = diff_q[0]
                
                
                r_dec = self.q.decode([r[i]])
                r_rec[i] = r_dec[0] + pred

        return r

    def decode(self, s):
        r = np.zeros(len(s))
        s_dec = s[:len(self.w)]
        s_dec = np.append(s_dec, self.q.decode(s[len(self.w):]))
        
        for i in range(len(s)):
            if(i < len(self.w)):
                r[i] = s[i]
            else:
                pred = 0
                for j in range(len(self.w)):
                    pred += self.w[j] * r[i-j-1]
                
                #print("r["+str(i) +"]: "+ str(pred) + " + " + str(s_dec[i]))
                    
                diff = s_dec[i]
                r[i] = pred + diff 
        return r

if __name__ == "__main__":
    signal1 = np.array([10.3, 7.1, 5.2, 3.7, 2.5, 2.0, 1.7, 1.3, 1.1])
    signal2 = np.array([1, 4, 1, 4, 1, 4, 1, 4, 1, 4, 1, 4])
    
    signal = signal1
    print("ORIG: " + str(signal))

    
    Q1 = UniformQuantizer(-5, 5, 5)
    C1 = Compander(2, -5, 5, 5) 

    DE1 = DifferentialEncoder(Q1)
    DE2 = DiffEnc(C1, [.2, .5])

    #print("DE1: " + str(DE1.encode(signal)))
    print("DE2 enc.: " + str(DE2.encode(signal)))

    signal_decoded = DE1.decode(DE1.encode(signal)) # encode and decode the signal
    #print("DE1: " + str(signal_decoded))

    signal_decoded2 = DE2.decode(DE2.encode(signal))
    print("DE2 dec.: " + str(signal_decoded2))

    pl.plot(signal, 'r', alpha=0.5, label="Original signal")
    pl.plot(signal_decoded, 'b', alpha=0.7, label="Differential Encoder")
    pl.plot(signal_decoded2, 'g', alpha=0.7, label="Linear differential Encoder")
    pl.legend()

    print("DE1-error: " + str(math.sqrt(sum((signal-signal_decoded)**2))))
    print("DE2-error: " + str(math.sqrt(sum((signal-signal_decoded2)**2))))
    pl.show()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
