#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from tut07_2016 import UniformQuantizier
import matplotlib.pyplot as pl

class DifferentialEncoder(object):

    def __init__(self, quantizer):
        self.q = quantizer

    def encode(self, s):
        r = np.zeros(len(s))
        diff = s[1:len(s)] - s[0:len(s)-1]
        r[1:len(s)] = self.q.encode(diff)
        r[0] = s[0]

        return r

    def encode2(self, s):
        r = np.zeros(len(s))
        r_rec = np.zeros(len(s))

        for i in range(len(s)):
            if i == 0:
                r[0] = s[0]
                r_rec[0] = s[0]
            else:
                diff = s[i] - r_rec[i-1]
                diff_q = self.q.encode([diff])
                r[i] = diff_q[0]

                diff_dec = self.q.decode([r[i]])
                r_rec[i] = r_rec[i-1] + diff_dec[0]

        return r

    def decode(self, s):
        r = np.zeros(len(s))
        s_dec = self.q.decode(s[1:len(s)])
        r[0] = s[0]
        for i in range(1,len(s)):
            r[i] = r[i-1] + s_dec[i-1]
        return r

if __name__ == "__main__":
    #signal = np.array([ 6.2, 9.7, 13.2, 5.9, 8, 7.4, 4.2, 1.8 ])
    signal = 20* np.random.rand(10000)

    Q1 = UniformQuantizier(-6, 6, 7)
    DE1 = DifferentialEncoder(Q1)

    #print DE1.encode(signal)

    signal_decoded = DE1.decode(DE1.encode(signal)) # encode and decode the signal
    print(signal_decoded)

    signal_decoded2 = DE1.decode(DE1.encode2(signal))
    print(signal_decoded2)

    pl.plot(signal - signal_decoded, 'r', alpha=0.7)
    pl.plot(signal - signal_decoded2, 'g', alpha=0.7)
    #pl.plot(signal, 'b', alpha=0.7)
    pl.show()
