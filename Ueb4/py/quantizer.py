#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import math
import matplotlib.pyplot as pl

def frange(x, y, jump):
  while x < y:
    yield x
    x += jump

class Quantizer(object):

    def __init__(self, partition=[], codebook=[]):
        self.partition = partition  # assume sorted
        self.codebook = codebook

    def encode(self, s):
        y = []
        for x in s:
            for i in range(len(self.partition)+1):
                if x <= self.partition[i]:
                    y.append(i)
                    break
        return y

    def decode(self, c):
        #print("c: " +str(c))
        #print("bk:" + str(self.codebook))
        s = []
        for x in c:
            s.append(self.codebook[x])
        return s

    def mse(self, s):
        dec = self.decode(self.encode(s))
        return np.sum((s - dec)**2)/float(len(s))
        
    def name(self):
        return "Manual"


class UniformQuantizer(Quantizer):

    def __init__(self, bin_min, bin_max, num_bins):
        self.codebook = np.linspace(bin_min, bin_max, num_bins)
        delta = (bin_max - bin_min) / (num_bins - 1)
        self.partition = self.codebook + delta/2.0
        self.partition[num_bins - 1] = np.inf
        
    def name(self):
        return "Uniform"
        
class Compander_alt(Quantizer):
    def __init__(self, mu, bin_min, bin_max, num_bins):
        self.codebook = np.array([])
        for x in np.linspace(-1, 1, num_bins):
            f = math.copysign(1, x) * (math.log(1 + mu * math.fabs(x))/math.log(1 + mu))
            #print(f)
            self.codebook = np.append(self.codebook,f)
            
        self.codebook = self.codebook * ((bin_max - bin_min)/2.)
        self.codebook += bin_min - self.codebook[0]
        
        self.partition = np.array([])
        for i in range(num_bins-1):
            mean = (self.codebook[i] + self.codebook[i + 1]) / 2.
            self.partition = np.append(self.partition, mean)
        self.partition = np.append(self.partition, np.inf)
        
    def name(self):
        return "Expander"
            
class Compander(Quantizer):
    def __init__(self, mu, bin_min, bin_max, num_bins):
        self.codebook = np.array([])
        for y in np.linspace(-1, 1, num_bins):
            f = math.copysign(1, y) * (1 / mu) * ((1+mu)**(math.fabs(y)) - 1)
            #print(f)
            self.codebook = np.append(self.codebook,f)
            
        self.codebook = self.codebook * ((bin_max - bin_min)/2.)
        self.codebook += bin_min - self.codebook[0]
        #print(self.codebook)
        
        self.partition = np.array([])
        for i in range(num_bins-1):
            mean = (self.codebook[i] + self.codebook[i + 1]) / 2.
            self.partition = np.append(self.partition, mean)
        self.partition = np.append(self.partition, np.inf)
        
    def name(self):
        return "Compander"

if __name__ == '__main__':
    s1 = np.linspace(-1, 11, 101)
        
    p1 = [-1, 1, np.inf]
    c1 = [-0.5, 0, 0.5]
    Q1 = Quantizer(p1, c1)
    #print(s1)
    keys = Q1.encode(s1)
    #print(keys)
    #print(Q1.decode(keys))

    #print("Partition:" + str(Q1.partition))
    #print("Codebook: " + str(Q1.codebook))
    
    UQ1 = UniformQuantizer(0, 10, 21)
    
    CP1 = Compander (1  , 0, 10, 21)
    CP2 = Compander (5  , 0, 10, 21)
    CP3 = Compander (255, 0, 10, 21)
    
    #print(UQ1.codebook)

    sUQ1 = UQ1.decode(UQ1.encode(s1))
    sCP1 = CP1.decode(CP1.encode(s1))
    sCP2 = CP2.decode(CP2.encode(s1))
    sCP3 = CP3.decode(CP3.encode(s1))
    
    if(False):
        pl.plot(s1, '--', alpha=.5)
        pl.plot(sUQ1, label="UQ")
        pl.plot(sCP1, label="CP, mu 1")
        pl.plot(sCP2, label="CP, mu 5")
        pl.plot(sCP3, label="CP, mu 255")
    elif(True):
        pl.plot(s1 - s1  , '--', alpha=.5)
        pl.plot(s1 - sUQ1, label="UQ", alpha =.7)
        #pl.plot(s1 - sCP1, label="CP, mu 1")
        pl.plot(s1 - sCP2, label="CP, mu 5", alpha =.7)
        pl.plot(s1 - sCP3, label="CP, mu 255", alpha =.7)
    else:
        pl.plot(CP1.codebook, label="CP, mu 1", alpha =.7)
        pl.plot(CP2.codebook, label="CP, mu 5", alpha =.7)
        pl.plot(CP3.codebook, label="CP, mu 255", alpha =.7)
        pl.plot(UQ1.codebook, label="UQ", alpha =.7)
        
    pl.legend()
    pl.show()
    
    
