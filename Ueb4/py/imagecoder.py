import coder
import quantizer

import math
import matplotlib.pyplot as pl
import numpy
import scipy.misc


def entropy(xs):
    ret = 0
    occurences = {}
    for x in xs:
        if x in occurences:
            occurences[x] += 1
        else:
            occurences[x] = 1

    probabilities = {}
    for c, oc in occurences.items():
        probabilities[c] = oc/len(xs)

    for c, p in probabilities.items():
        ret += p*(-math.log(p, 2))
    return ret


def main():
    entropies = {}
    image2d = scipy.misc.imread("Lenna.png", True)
    imageDimensions = len(image2d), len(image2d[0])
    image = numpy.array(numpy.reshape(image2d, (1, -1))[0])

    print("Entropy source image: " + str(entropy(image)))

    interval = [2, 5, 7]
    cs = [ quantizer.Compander(255, -255, 255, i) for i in interval]
    cs.extend([quantizer.UniformQuantizer(-255, 255, i) for i in interval])

    meanErrorCompander = []
    meanErrorUniform = []

    run = 1
    for c in cs:
        print("Round " + str(run) + " of " + str(len(cs)))
        run += 1
        DE1 = coder.DiffEnc(c, [0.3, 0.3, 0.4])
        code = DE1.encode(image)
        processedImage = DE1.decode(code)
        #print(c.codebook)
        #print(c.partition)
        #print(code)
        #print(processedImage)

        meanError = sum((processedImage - image)**2) / len(image)
        print("Mean squared error: " + str(meanError))
        print("Entropy: " + str(entropy(processedImage)))
        print("Rate: " + str(math.log(len(c.codebook), 2)))
        

        if run-1 <= len(cs)/2:
            meanErrorCompander.append(meanError)
        else:
            meanErrorUniform.append(meanError)
            
        entropies[len(c.codebook)] = entropy(processedImage)

        finalImage = numpy.reshape(processedImage, imageDimensions)
        scipy.misc.imsave("Lenna_" + c.name() + "_" + str(len(c.codebook)) + ".png", finalImage)

    pl.plot(meanErrorCompander, 'b', label="Compander")
    pl.plot(meanErrorUniform, 'g', label="Uniform")
    pl.legend()
    pl.show()

    # for interval, entropy_ in entropies.items():  # Because python's scoping is so awesome...
    #     print("Entropy for Interval %s: %s" % (interval, entropy_))

if __name__ == "__main__":
    main()
