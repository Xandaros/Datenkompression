#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np


class Quantizer(object):

    def __init__(self, partition=[], codebook=[]):
        self.partition = partition  # assume sorted
        self.codebook = codebook

    def encode(self, s):
        y = []
        for x in s:
            for i in range(len(self.partition)+1):
                if x <= self.partition[i]:
                    y.append(i)
                    break
        return y

    def decode(self, c):
        #print("c: " +str(c))
        #print("bk:" + str(self.codebook))
        s = []
        for x in c:
            s.append(self.codebook[x])
        return s

    def mse(self, s):
        dec = self.decode(self.encode(s))
        return np.sum(np.power(s - dec, 2))/float(len(s))


class UniformQuantizier(Quantizer):

    def __init__(self, bin_min, bin_max, num_bins):
        self.codebook = np.linspace(bin_min, bin_max, num_bins)
        delta = (bin_max - bin_min) / (num_bins - 1)
        self.partition = self.codebook + delta/2.0
        self.partition[num_bins - 1] = np.inf


if __name__ == '__main__':
    s1 = np.array([1,2,3,4,42])
    p1 = [0, 1, np.inf]
    c1 = [-0.5, 0.5, 1.5]

    Q1 = Quantizer(p1, c1)
    print(s1)
    print(Q1.decode(Q1.encode(s1)))

    UQ1 = UniformQuantizier(-1, 1, 3)
    print(UQ1.codebook)
    print(UQ1.partition)

    print(UQ1.mse(s1))
